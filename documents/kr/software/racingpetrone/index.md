### Racing Petrone

---

<div align="center">

    <table>
        <tr>
            <td colspan="3">
                <div align="center">
                    Racing Petrone
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <div align="center">
                    <img src="/assets/images/products/racingpetrone.jpg" alt="Racing Petrone">
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div align="center">
                    Support
                </div>
            </td>
            <td>
                <div align="center">
                    <img src="/assets/images/products/drone_fighter_and_controller.jpg" alt="drone_fighter_and_controller" height="120" width="120"><br>
                    DRONE FIGHTER EDU
                </div>
            </td>
            <td>
                <div align="center">
                    <img src="/assets/images/products/petrone_v2_and_controller.jpg" alt="petrone_v2_and_controller" height="120" width="120"><br>
                    PETRONE V2
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div align="center">
                    Version
                </div>
            </td>
            <td colspan="2">
                <div align="center">
                    V3.5
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div align="center">
                    Download
                </div>
            </td>
            <td colspan="2">
                <div align="center">
                    <a href="https://s3.ap-northeast-2.amazonaws.com/byrobot/RacingPetrone_v3.5.exe" target="_blank">Windows</a>,&nbsp;
                    <a href="https://s3.ap-northeast-2.amazonaws.com/byrobot/Racing+Petrone_V3.5.dmg" target="_blank">MacOS</a>
                </div>
            </td>
        </tr>
    </table>

</div>

---

Modified : 2018.9.17