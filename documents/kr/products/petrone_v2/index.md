### PETRONE V2

---

<div align="center">
    <table>
        <tr>
            <td colspan="3">
                <div align="center">
                    <img src="/assets/images/products/petrone_v2_and_controller.jpg" alt="petrone_v2">
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="3"><div align="center">&nbsp;<br>Firmware<br>&nbsp;</div></td>
        </tr>
        <tr>
            <td rowspan="2"><div align="center">Version</div></td>
            <td>
                <div align="center">Drone</div>
            </td>
            <td>
                <div align="center">1.3.R.2</div>
            </td>
        </tr>
        <tr>
            <td>
                <div align="center">Controller</div>
            </td>
            <td>
                <div align="center">1.3.R.2</div>
            </td>
        </tr>
        <tr>
            <td><div align="center">Release Date</div></td>
            <td colspan="2"><div align="center">2018.7.23</div></td>
        </tr>
        <tr>
            <td><div align="center">Download</div></td>
            <td colspan="2">
                <div align="center"><a href="https://drive.google.com/open?id=1wVFreiSFHyedjRDVR3x3wTi1GA88khEc" target="_blank">Windows</a>,&nbsp;<a href="https://s3.ap-northeast-2.amazonaws.com/byrobot/PetroneV2_20180723_release_1.3_3.zip" target="_blank">Windows(2)</a></div>
            </td>
        </tr>
        <tr>
            <td><div align="center">Update Log</div></td>
            <td colspan="2">
                <div align="center"><a href="/documents/kr/products/petrone_v2/log/updates/firmware/">한국어</a></div>
            </td>
        </tr>
        <tr>
            <td><div align="center">Update Manual</div></td>
            <td colspan="2">
                <div align="center"><a href="/documents/kr/products/petrone_v2/manual/update/">한국어</a></div>
            </td>
        </tr>
        <tr>
            <td colspan="3"><div align="center">&nbsp;<br>Driver<br>&nbsp;</div></td>
        </tr>
        <tr>
            <td>
                <div align="center">Download</div>
            </td>
            <td colspan="2">
                <div align="center"><a href="https://drive.google.com/open?id=1HisAPi3nipnnyuFklNXiKn46cV_5P0iy" target="_blank">Win7, Win8 (32bit)</a></div>
                <div align="center"><a href="https://drive.google.com/open?id=1Cm7fIt9XAi-dUNnqxVblNriL8oVfqekg" target="_blank">Win7, Win8 (64bit)</a></div>
                <div align="center">Win 10은 자동 인식</div>
            </td>
        </tr>
        <tr>
            <td colspan="3"><div align="center">&nbsp;<br>Documents<br>&nbsp;</div></td>
        </tr>
        <tr>
            <td><div align="center">Protocol</div></td>
            <td colspan="2">
                <div align="center"><a href="/documents/kr/products/petrone_v2/protocol/">한국어</a>,&nbsp;<a href="/documents/en/products/petrone_v2/protocol/">English</a></div>
            </td>
        </tr>
        <tr>
            <td><div align="center">Python Library</div></td>
            <td colspan="2">
                <div align="center"><a href="/documents/kr/products/petrone_v2/library/python/petrone_v2/">한국어</a></div>
            </td>
        </tr>
        <tr>
            <td><div align="center">Entry Examples</div></td>
            <td colspan="2">
                <div align="center"><a href="/documents/kr/products/petrone_v2/entry/examples/">한국어</a>,&nbsp;<a href="/documents/en/products/petrone_v2/entry/examples/">English</a></div>
            </td>
        </tr>
        <tr>
            <td colspan="3"><div align="center">&nbsp;<br>Repository<br>&nbsp;</div></td>
        </tr>
        <tr>
            <td><div align="center">Windows</div></td>
            <td colspan="2">
                <div align="center">
                    <a href="https://github.com/petrone-v2/window_console_example_for_petrone_v2" target="_blank">Consol Example</a>
                </div>
            </td>
        </tr>
    </table>
</div>

---

Modified : 2018.10.5