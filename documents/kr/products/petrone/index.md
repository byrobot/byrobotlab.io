### PETRONE

---

<div align="center">
    <table>
        <tr>
            <td colspan="3">
                <div align="center">
                    <img src="/assets/images/products/petrone.jpg" alt="petrone">
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="3"><div align="center">&nbsp;<br>Firmware<br>&nbsp;</div></td>
        </tr>
        <tr>
            <td rowspan="3"><div align="center">Version</div></td>
            <td><div align="center">Drone Main</div></td>
            <td><div align="center">50</div></td>
        </tr>
        <tr>
            <td><div align="center">Drone Sub</div></td>
            <td><div align="center">18</div></td>
        </tr>
        <tr>
            <td><div align="center">Link</div></td>
            <td><div align="center">17</div></td>
        </tr>
        <tr>
            <td rowspan="2"><div align="center">Release Date</div></td>
            <td><div align="center">Drone</div></td>
            <td><div align="center">2018.5.31</div></td>
        </tr>
        <tr>
            <td><div align="center">Link</div></td>
            <td><div align="center">2018.3.5</div></td>
        </tr>
        <tr>
            <td><div align="center">Download</div></td>
            <td colspan="2">
                <div align="center"><a href="https://drive.google.com/open?id=1GkjdZaI1P0CaDn6RZDYJ9-ZNmt5Onkp-" target="_blank">Windows</a>,&nbsp;<a href="https://s3.ap-northeast-2.amazonaws.com/byrobot/PetroneLink_20180305_release_0.zip" target="_blank">Windows(2)</a></div>
            </td>
        </tr>
        <tr>
            <td><div align="center">Update Log</div></td>
            <td colspan="2"><div align="center"><a href="/documents/kr/products/petrone/log/updates/firmware/">한국어</a></div></td>
        </tr>
        <tr>
            <td><div align="center">Update Manual</div></td>
            <td colspan="2">
                <div align="center"><a href="/documents/kr/products/petrone/manual/update/">한국어</a></div>
            </td>
        </tr>
        <tr>
            <td colspan="3"><div align="center">&nbsp;<br>Driver<br>&nbsp;</div></td>
        </tr>
        <tr>
            <td><div align="center">Download</div></td>
            <td colspan="2">
                <div align="center"><a href="https://www.silabs.com/documents/public/software/CP210x_Windows_Drivers.zip" target="_blank">CP210x Windows Drivers<br>(for Petrone Link)</a></div>
            </td>
        </tr>
        <tr>
            <td colspan="3"><div align="center">&nbsp;<br>Documents<br>&nbsp;</div></td>
        </tr>
        <tr>
            <td><div align="center">Protocol</div></td>
            <td colspan="2">
                <div align="center"><a href="/documents/kr/products/petrone/protocol/">한국어</a>,&nbsp;<a href="/documents/en/products/petrone/protocol/">English</a></div>
            </td>
        </tr>
        <tr>
            <td><div align="center">Python Library</div></td>
            <td colspan="2">
                <div align="center"><a href="/documents/kr/products/petrone/library/python/petrone/">한국어</a>,&nbsp;<a href="/documents/en/products/petrone/library/python/petrone/">English</a></div>
            </td>
        </tr>
        <tr>
            <td colspan="3"><div align="center">&nbsp;<br>Repository<br>&nbsp;</div></td>
        </tr>
        <tr>
            <td>
                <div align="center">Android</div>
            </td>
            <td colspan="2">
                <div align="center"><a href="https://github.com/petrone/PetroneAPI_kotlin" target="_blank">PetroneAPI_kotlin</a></div>
            </td>
        </tr>
        <tr>
            <td>
                <div align="center">iOS</div>
            </td>
            <td colspan="2">
                <div align="center">
                    <a href="https://github.com/petrone/PetroneAPI_swift" target="_blank">PetroneAPI_swift</a><br>
                    <a href="https://github.com/petrone/PetroneAPI_swift_Sample" target="_blank">PetroneAPI_swift_Sample</a>
                </div>
            </td>
        </tr>
    </table>
</div>

---

Modified : 2018.10.5