### Drone Fighter

---

<div align="center">
    <table>
        <tr>
            <td colspan="4">
                <div align="center">
                    <img src="/assets/images/products/drone_fighter_and_controller.jpg" alt="drone_fighter">
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="4"><div align="center">&nbsp;<br>Firmware<br>&nbsp;</div></td>
        </tr>
        <tr>
            <td rowspan="4"><div align="center">Version</div></td>
            <td rowspan="2"><div align="center">General</div></td>
            <td><div align="center">Drone</div></td>
            <td><div align="center">4.2</div></td>
        </tr>
        <tr>
            <td><div align="center">Controller</div></td>
            <td><div align="center">4.2</div></td>
        </tr>
        <tr>
            <td rowspan="2"><div align="center">Education</div></td>
            <td><div align="center">Drone</div></td>
            <td><div align="center">1703</div></td>
        </tr>
        <tr>
            <td><div align="center">Controller</div></td>
            <td><div align="center">1802</div></td>
        </tr>
        <tr>
            <td><div align="center">Release Date</div></td>
            <td colspan="3"><div align="center">2018.5.4</div></td>
        </tr>
        <tr>
            <td><div align="center">Download</div></td>
            <td colspan="3">
                <div align="center"><a href="https://drive.google.com/open?id=1Iu085RiTYxA8CBpZ80ZGDym7qCj0ETyy" target="_blank">Windows</a>,&nbsp;<a href="https://s3.ap-northeast-2.amazonaws.com/byrobot/DroneFighter_20180504_release_4.zip" target="_blank">Windows(2)</a></div>
            </td>
        </tr>
        <tr>
            <td><div align="center">Update Log</div></td>
            <td colspan="3"><div align="center"><a href="/documents/kr/products/dronefighter2017/log/updates/firmware/">한국어</a></div></td>
        </tr>
        <tr>
            <td><div align="center">Update Manual</div></td>
            <td colspan="3"><div align="center"><a href="/documents/kr/products/dronefighter2017/manual/update/">한국어</a></div></td>
        </tr>
        <tr>
            <td colspan="4"><div align="center">&nbsp;<br>Driver<br>&nbsp;</div></td>
        </tr>
        <tr>
            <td><div align="center">Download</div></td>
            <td colspan="3">
                <div align="center"><a href="https://drive.google.com/open?id=19bmT3b8a3nEqCXzXk88lMeO7gHxyGZuY" target="_blank">Win7, Win8</a></div>
                <div align="center">Win 10은 자동 인식</div>
            </td>
        </tr>
        <tr>
            <td colspan="4"><div align="center">&nbsp;<br>Documents<br>&nbsp;</div></td>
        </tr>
        <tr>
            <td><div align="center">Protocol</div></td>
            <td colspan="3">
                <div align="center"><a href="/documents/kr/products/dronefighter2017/protocol/">한국어</a>,&nbsp;<a href="/documents/en/products/dronefighter2017/protocol/">English</a></div>
            </td>
        </tr>
    </table>
</div>

---

Modified : 2018.10.5