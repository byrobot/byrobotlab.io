### E-Drone

---

<div align="center">
    <table>
        <tr>
            <td colspan="3">
                <div align="center">
                    <img src="/assets/images/products/e_drone.jpg" alt="e_drone">
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="3"><div align="center">&nbsp;<br>Firmware<br>&nbsp;</div></td>
        </tr>
        <tr>
            <td rowspan="2"><div align="center">Version</div></td>
            <td><div align="center">Drone</div></td>
            <td><div align="center">0.2.16</div></td>
        </tr>
        <tr>
            <td><div align="center">Controller</div></td>
            <td><div align="center">0.2.14</div></td>
        </tr>
        <tr>
            <td><div align="center">Release Date</div></td>
            <td colspan="2"><div align="center">2018.10.16</div></td>
        </tr>
        <tr>
            <td><div align="center">Download</div></td>
            <td colspan="2"><div align="center"><a href="https://drive.google.com/open?id=1EOwzym4Ncf7neTeluddCu6258EufrfwV" target="_blank">Windows</a></div></td>
        </tr>
        <tr>
            <td><div align="center">Update Log</div></td>
            <td colspan="2"><div align="center"><a href="/documents/kr/products/e_drone/log/updates/firmware/">한국어</a></div></td>
        </tr>
        <tr>
            <td><div align="center">Update Manual</div></td>
            <td colspan="2"><div align="center"><a href="/documents/kr/products/e_drone/manual/update/">한국어</a></div></td>
        </tr>
        <tr>
            <td colspan="3"><div align="center">&nbsp;<br>Documents<br>&nbsp;</div></td>
        </tr>
        <tr>
            <td><div align="center">User Manual</div></td>
            <td colspan="2"><div align="center"><a href="/documents/kr/products/e_drone/manual/user/">한국어</a></div></td>
        </tr>
        <tr>
            <td><div align="center">Protocol</div></td>
            <td colspan="2"><div align="center"><a href="/documents/kr/products/e_drone/protocol/">한국어</a></div></td>
        </tr>
        <tr>
            <td><div align="center">Python Library</div></td>
            <td colspan="2"><div align="center"><a href="/documents/kr/products/e_drone/library/python/e_drone/">한국어</a></div></td>
        </tr>
    </table>
</div>

---

Modified : 2018.10.16